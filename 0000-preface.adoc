////
Les supports de Formatux sont publiés sous licence Creative Commons-BY-SA et sous licence Art Libre.
Vous êtes ainsi libre de copier, de diffuser et de transformer librement les œuvres dans le respect des droits de l’auteur.

    BY : Paternité. Vous devez citer le nom de l’auteur original.
    SA : Partage des Conditions Initiales à l’Identique.

Licence Creative Commons-BY-SA : https://creativecommons.org/licenses/by-sa/3.0/fr/
Licence Art Libre : http://artlibre.org/

Auteurs : Patrick Finet, Xavier Sauvignon, Antoine Le Morvan
////

[preface]
== Préface

GNU/Linux est un **système d'exploitation** libre fonctionnant sur la base d'un **noyau Linux**, également appelé **kernel Linux**.

Linux est une implémentation libre du système **UNIX** et respecte les spécifications **POSIX**.

GNU/Linux est généralement distribué dans un ensemble cohérent de logiciels, assemblés autour du noyau Linux et prêt à être installé. Cet ensemble porte le nom de “**Distribution**”.

*  La plus ancienne des distributions est la distribution **Slackware**.
*  Les plus connues et utilisées sont les distributions **Debian**, **RedHat** et **Arch**, et servent de base pour d'autres distributions comme **Ubuntu**, **CentOS**, **Fedora**, **Mageia** ou **Manjaro**.

Chaque distribution présente des particularités et peut être développée pour répondre à des besoins très précis :

*  services d'infrastructure ;
*  pare-feu ;
*  serveur multimédia ;
*  serveur de stockage ;
* etc.

La distribution présentée dans ces pages est principalement la CentOS, qui est le pendant gratuit de la distribution RedHat, mais la plupart des supports s'appliquent généralement aussi à Ubuntu et Debian. La distribution CentOS est particulièrement adaptée pour un usage sur des serveurs d'entreprises.

=== Crédits

Ce support de cours a été rédigé sur plusieurs années par de nombreux formateurs.

Les formateurs à l'origine du projet et contributeurs principaux actuels :

* **Antoine Le Morvan** ;
* **Xavier Sauvignon** ;

Notre relecteur principal des premières versions :

* **Patrick Finet** ;

Il a rédigé la partie Git :

* **Carl Chenet**

Ils ont contribués à la rédaction :

* **Nicolas Kovacs** : Apache sous CentOS7 ;
* ...

Enfin, les illustrations de qualités sont dessinées pour formatux par **François Muller** (aka `Founet`).

=== L'histoire de Formatux

Nous étions (Xavier, Antoine et Patrick) tous les trois formateurs dans la même école de formation pour adulte.

L'idée de fournir aux stagiaires un support en PDF reprenant la totalité des cours dispensés pour leur permettre de réviser et approfondir les points vus en classe pendant la journée nous a rapidement paru être une priorité.

En décembre 2015, nous testions les solutions qui existaient pour rédiger un support d'une telle taille, et nous avons retenu dès le début du projet le format Asciidoc pour sa simplicité et le générateur Asciidoctor pour la qualité du support généré, la possibilité de personnaliser le rendu mais surtout pour l'étendue de ses fonctionnalités. Nous avons également testé le markdown, mais avons été plus rapidement limité.

[NOTE]
====
5 ans après le début du projet et après avoir basculé notre site web sous Antora, nous ne regrettons absolument pas le choix technique d'Asciidoc.
====

La gestion des sources a été confiée dès l'année suivante à la forge Gitlab de Framagit, ce qui nous permettait de travailler à plusieurs en même temps sur le support, et de faciliter la relecture du support par Patrick. En découvrant la CI de gitlab-ci, nous avions enfin la stack qui nous permettrait d'automatiser totalement la génération du support.

Le travail de rédaction étant fortement personnel, sachant que nous risquions d'être muté rapidement dans les années à suivre, nous avons voulu ce support sous Licence Libre, afin qu'un maximum de personnes puissent à la fois contribuer et en profiter, et que notre beau support ne se perde pas.

Même après avoir tous quitté notre organisme de formation, nous avons continué à faire vivre le projet formatux, en faisant évoluer certaines parties, en nous appuyant sur d'autres pour nos formations et en intégrant de nouvelles parties, comme la partie Git de Carl Chenet.

En juillet 2019, nous (Xavier et Antoine, Patrick étant parti à la retraite) avons décidé de reprendre le développement de Formatux plus activement et d'en faire un peu plus sa promotion. L'organisation complète du support a été revu, en le scindant en 6 dépôts disctincts, correspondant à chacune des parties, au support global ainsi qu'au site web. Nous avons voulu notre organisation full devops, afin que la génération de chacune des parties soient totalement automatisées et inter-dépendantes les unes des autres.

Il est difficile aujourd'hui d'évaluer la popularité de notre support. Ce dernier a longtemps été disponible en téléchargement par torrent sur freetorrent (malheureusement aujourd'hui disparu) et en direct depuis le site web. Nous n'avons pas de métriques et nous n'en voulons pas particulièrement. Nous retirons notre satisfaction dans les contacts que nous avons avec nos lecteurs.

=== Licence

Formatux propose des supports de cours Linux libres de droits à destination des formateurs ou des personnes désireuses d'apprendre à administrer un système Linux en autodidacte.

Les supports de Formatux sont publiés sous licence Creative Commons-BY-SA et sous licence Art Libre. Vous êtes ainsi libre de copier, de diffuser et de transformer librement les œuvres dans le respect des droits de l’auteur.

*BY* : Paternité. Vous devez citer le nom de l’auteur original.

*SA* : Partage des Conditions Initiales à l’Identique.

* Licence Creative Commons-BY-SA : https://creativecommons.org/licenses/by-sa/3.0/fr/
* Licence Art Libre : http://artlibre.org/

Les documents de Formatux et leurs sources sont librement téléchargeables sur formatux.org :

* https://www.formatux.org/

Les sources de nos supports sont hébergés chez Framasoft sur leur forge Framagit. Vous y trouverez les dépôts des codes sources à l'origine de la version de ce document :

* https://framagit.org/formatux/

A partir des sources, vous pouvez générer votre support de formation personnalisé. Nous vous recommandons le logiciel AsciidocFX téléchargeable ici : http://asciidocfx.com/ ou l'éditeur Atom avec les plugins Asciidoc.

=== Comment contribuer au projet ?

Si vous voulez participer à la rédaction des supports formatux,  **forkez-nous sur** https://framagit.org/formatux/[*framagit.org*].

Vous pourrez ensuite apporter vos modifications, compiler votre support personnalisé et nous proposer vos modifications.

Vous êtes les bienvenus pour :

* Compléter le document avec un nouveau chapitre,
* Corriger ou compléter les chapitres existants,
* Relire le document et corriger l'orthographe, la mise en forme,
* Promouvoir le projet

==== De votre côté

. Créer un compte sur https://framagit.org
. Créer un fork du projet que vous voulez modifier parmi la liste des projets du groupe : https://framagit.org/formatux/[Créer le fork]
. Créer une branche nommée develop/[Description]
    * Où [Description] est une description très courte de ce qui va être fait
. Faire des commits dans votre branche
. Pusher la branche sur votre fork
. Demander une merge request

Essayer de conserver le même ton qui est déjà employé dans le reste du support (pas de 'je' ni de smileys par exemple).

==== La suite se passe de notre côté

. Quelqu'un relira votre travail
    * Essayez de rendre ce travail plus facile en organisant vos commits
. S'il y a des remarques sur le travail, le relecteur fera des commentaires sur la merge request
. Si la merge request lui semble correcte il peut merger votre travail avec la branche **develop**

==== Corrections suite à une relecture

La relecture de la merge request peut vous amener à faire des corrections.
Vous pouvez faire ces corrections dans votre branche, ce qui aura pour effet de les ajouter à la merge request.

==== Comment compiler mon support formatux ?

Après avoir forké notre projet ou l'avoir cloné (`git clone https://framagit.org/group/formatux-PARTIEXX.git`), déplacez vous dans le dossier formatux-PARTIEXX nouvellement créé.

Vous avez ensuite plusieurs possibilités :

* Vous utilisez le logiciel AsciiDocFX ou Atom avec ses plugins Asciidoc (recommandé sous Windows) : lancez le logiciel, ouvrez le fichier .adoc désiré, et cliquez sur le bouton **PDF** (AsciidoctorFX) ou regarder la prévisualisation (Atom).

* Vous êtes sous Linux et vous avez déjà installé le paquet asciidoctor : exécutez la commande `asciidoctor-pdf -t -D public -o support.pdf support.adoc`.

==== Comment faire un don

Vous pouvez faire un don par paypal pour soutenir nos efforts. Ces dons serviront à payer notre nom de domaine et notre hébergement. Nous pourrons également reverser une partie de ces dons à Framasoft, qui héberge gracieusement nos repos et met à disposition un runner qui compile nos supports et notre site web. Enfin, une petite bière entre nous peut également être au programme.

Accès à la cagnotte paypal : https://www.paypal.com/pools/c/8hlM1Affp1.

==== Nous contacter

Nous sommes disponible sur gitter pour échanger autour de formatux, de Linux et de la formation : https://gitter.im/formatux-fr/formatux.

=== Antora

Pour la génération de notre site web, nous utilisons Antora. Antora nous permet, depuis les mêmes sources, de pouvoir générer le support en PDF et le site web statique en HTML. Le développement d'Antora est sponsorisé par OpenDevise Inc.
