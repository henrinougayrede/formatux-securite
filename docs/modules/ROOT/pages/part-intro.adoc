La sécurité sous LINUX.

Dans cette partie de notre support Formatux, nous ferons un tour d'horizon des aspects sécuritaire d'une distribution Linux :

* La gestion de l'endossement des droits su/sudo,
* Les modules d'authentification avec PAM,
* La sécurité avec SELinux,
* La gestion du firewall avec IPTables et Fail2Ban,
* La sécurisation du service d'administration à distance OpenSSH,
* La gestion d'une autorité de certification.

Bonne lecture.
